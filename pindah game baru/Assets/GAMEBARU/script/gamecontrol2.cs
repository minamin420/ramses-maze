﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gamecontrol2 : MonoBehaviour
{
    public static int nyawaplayer;
    public static int asulah2 = 6;

    public GameObject heart1, heart2, heart3, heart4, heart5;
    // Start is called before the first frame update
    void Start()
    {
        scorescript.skorbro = 0;
        nyawaplayer = asulah2;
        if (nyawaplayer > 6 && nyawaplayer <= 7)
        {
            nyawaplayer = 6;
        }

    }

    // Update is called once per frame
    void Update()
    {


        if (nyawaplayer < 0)
        {
            nyawaplayer = 0;
            PlayerPrefsManager.coins += (scorescript.skorbro / 10);
            SceneManager.LoadScene(sceneName: "Gameover2");

        }


        switch (nyawaplayer)
        {

            case 10:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                heart4.gameObject.SetActive(true);
                heart5.gameObject.SetActive(true);
                break;

            case 8:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                heart4.gameObject.SetActive(true);
                heart5.gameObject.SetActive(false);
                break;


            case 6:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                heart4.gameObject.SetActive(false);
                heart5.gameObject.SetActive(false);
                break;

            case 4:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                heart5.gameObject.SetActive(false);
                break;

            case 2:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                heart5.gameObject.SetActive(false);
                break;

            case 0:
                heart1.gameObject.SetActive(false);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                heart5.gameObject.SetActive(false);
                break;
        }

    }
}
