﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gamecontrol3 : MonoBehaviour
{
    public static int nyawaplayer;
    public static int asulah3 = 9;

    public GameObject heart1, heart2, heart3, heart4, heart5;
    // Start is called before the first frame update
    void Start()
    {
        scorescript.skorbro = 0;
        nyawaplayer = asulah3;
        if (nyawaplayer > 9 && nyawaplayer <= 10)
        {
            nyawaplayer = 9;
        }

    }

    // Update is called once per frame
    void Update()
    {


        if (nyawaplayer < 0)
        {
            nyawaplayer = 0;
            PlayerPrefsManager.coins += (scorescript.skorbro / 10);
            SceneManager.LoadScene(sceneName: "gameover3");

        }


        switch (nyawaplayer)
        {

            case 15:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                heart4.gameObject.SetActive(true);
                heart5.gameObject.SetActive(true);
                break;

            case 12:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                heart4.gameObject.SetActive(true);
                heart5.gameObject.SetActive(false);
                break;


            case 9:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                heart4.gameObject.SetActive(false);
                heart5.gameObject.SetActive(false);
                break;

            case 6:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                heart5.gameObject.SetActive(false);
                break;

            case 3:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                heart5.gameObject.SetActive(false);
                break;

            case 0:
                heart1.gameObject.SetActive(false);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                heart5.gameObject.SetActive(false);
                break;
        }

    }
}
