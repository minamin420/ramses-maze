﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onoffmusik : MonoBehaviour
{
    public AudioSource src;


    // Start is called before the first frame update



    private static onoffmusik instance = null;
    public static onoffmusik Instance
    {
        get { return instance; }
    }

    public void playmusic()
    {
        src.Play();
    }

    public void pausemusic()
    {
        src.Pause();
    }

    
}
