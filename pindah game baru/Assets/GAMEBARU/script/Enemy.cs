﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{


    public int health;
   // public float speed;

    public GameObject deathEffect;
    public GameObject Player;

    public float VitesseEnnemi;



    // Start is called before the first frame update


    void Start()
    {
       
        

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, VitesseEnnemi * Time.deltaTime);
        if (health <= 0)
        {
            Instantiate(deathEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
            scorescript.skorbro += 10;

        }
        //transform.Translate(Vector2.down * speed * Time.deltaTime);
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "playerspaceship")
        {
            GameControl.nyawaplayer -= 1;
            gamecontrol2.nyawaplayer -= 1;
            gamecontrol3.nyawaplayer -= 1;
        }
    }
    public void TakeDamage(int damage)
    {
       

        health -= damage;
        Debug.Log("damage TAKEN");
    }
}
