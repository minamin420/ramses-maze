﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{

    public int health;
    public float speed;

    public GameObject deathEffect;




    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "playerspaceship")
        {
            GameControl.nyawaplayer -= 1;
            gamecontrol2.nyawaplayer -= 1;
            gamecontrol3.nyawaplayer -= 1;
        }
    }

    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {

        
        transform.Translate(Vector2.down * speed * Time.deltaTime);

    }

    public void TakeDamage(int damage)
    {


        health -= damage;
        Debug.Log("damage TAKEN");
    }
}
