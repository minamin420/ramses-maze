﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{


    private Rigidbody2D rb;
    public float speed;
    public float jumpForce;
    private float moveInput;

    private Animator anim;


    //nyawa



   
    public Transform feetPos;
    public float checkRadius;
    public LayerMask WhatIsGround;

    public float JumpTimeCounter;
    public float JumpTime;
    private bool isJumping;

    // Start is called before the first frame update

    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
  


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        moveInput = CrossPlatformInputManager.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

    }

    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Jump"))
            Jump();

       

        if(moveInput > 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (moveInput < 0)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        

            
        
        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }
    }

    void Jump()
    {
        isJumping = true;
        JumpTimeCounter = 1;
        rb.velocity = Vector2.up * jumpForce;
       
    }

        

    private void NewMethod()
    {
        JumpTimeCounter -= Time.deltaTime;
    }

}
